package com.example.jpabuddyrestcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaBuddyRestCrudApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpaBuddyRestCrudApplication.class, args);
    }

}
