package com.example.jpabuddyrestcrud.entities;

import javax.persistence.*;

@Entity
@Table(name = "app_user", indexes = {
        @Index(name = "idx_appuser_name", columnList = "name")
})
public class AppUser {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "password")
    private String password;

    public AppUser(Long id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public AppUser() {}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" +
                "id = " + id + ", " +
                "name = " + name + ", " +
                "password = " + password + ")";
    }
}