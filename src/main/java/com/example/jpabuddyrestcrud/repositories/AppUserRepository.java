package com.example.jpabuddyrestcrud.repositories;

import com.example.jpabuddyrestcrud.entities.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "users", path = "users")
public interface AppUserRepository extends JpaRepository<AppUser, Long> {
    List<AppUser> findByName(String name);
}