package com.example.jpabuddyrestcrud;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.reactive.server.WebTestClient;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DisplayName("E2E Integration tests")
class JpaBuddyRestCrudApplicationTests {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    private WebTestClient testClient;

    @Test
    @DisplayName("TestRestTemplate: it should return response status code 200 and body")
    void itShouldReturnResponseOkAndBodyRT() {
        // Arrange
        String url = "http://localhost:" + port;
        // Act
        ResponseEntity<String> response
                = testRestTemplate.getForEntity(url, String.class);
        // Assert
        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).isNotEmpty();
    }

    @Test
    @DisplayName("WebTestClient: it should return response status code 200 and body")
    void itShouldReturnResponseOkAndBodyWC() {
        // Arrange
        String url = "http://localhost:" + port;
        String uri = "/profile";
        // Act -Assert
        WebTestClient testClient = WebTestClient
                .bindToServer()
                .baseUrl(url)
                .build();

        testClient.get().uri(uri)
                .accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody();
    }

}
