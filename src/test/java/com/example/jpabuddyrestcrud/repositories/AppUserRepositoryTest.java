package com.example.jpabuddyrestcrud.repositories;

import com.example.jpabuddyrestcrud.entities.AppUser;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.data.domain.Sort.Direction.DESC;

@ActiveProfiles("dev")
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DisplayName("AppUserRepositoryTest: tests with H2 database in memory")
class AppUserRepositoryTest {

    @Autowired
    private AppUserRepository underTest;

    @Autowired
    private TestEntityManager entityManager;

    private Long saveUser(AppUser user) {
        entityManager.persist(user);
        return user.getId();
    }

    private void removeUser(AppUser user) {
        entityManager.remove(user);
    }

    private AppUser findUser(AppUser user) {
        return entityManager.find(AppUser.class, user.getId());
    }

    private boolean isUserExists(AppUser user) {
        return findUser(user) != null;
    }


    @Test
    @DisplayName("It should find all users")
    void itShouldFindAllUsers() {
        // Arrange
        AppUser john = new AppUser(11L, "John", "secret");
        Long johnId = saveUser(john);
        // Act
        List<AppUser> users = underTest.findAll();
        // Assert
        assertAll(
                () -> assertTrue(users.size() > 0),
                () -> assertEquals(john.getId(), johnId)
        );
        // CleanUp
        removeUser(john);
    }

    @Test
    @DisplayName("It should find user by id")
    void itShouldFindUserById() {
        // Arrange
        AppUser kate = new AppUser(11L, "Kate", "secret");
        Long kateId = saveUser(kate);
        // Act
        Optional<AppUser> userOptional = underTest.findById(kateId);
        AppUser user = userOptional.orElseThrow(EntityNotFoundException::new);
        // Assert
        assertAll(
                () -> assertEquals(kate.getId(), user.getId()),
                () -> assertEquals(kate.getName(), user.getName()),
                () -> assertEquals(kate.getPassword(), user.getPassword())
        );
        // CleanUp
        removeUser(kate);
    }

    @Test
    @DisplayName("It should find user by name")
    void itShouldFindUserByName() {
        // Arrange
        AppUser bill = new AppUser(11L, "Bill Kid", "secret");
        Long billId = saveUser(bill);
        // Act
        List<AppUser> users = underTest.findByName(bill.getName());
        AppUser user = users.stream().filter(u -> Objects.equals(u.getId(), billId)).findFirst().orElseThrow(EntityNotFoundException::new);
        // Assert
        assertEquals(billId, user.getId());
        // CleanUp
        removeUser(bill);
    }

    @Test
    @DisplayName("It should delete user by id")
    void itShouldDeleteUserById() {
        // Arrange
        AppUser john = new AppUser(22L, "John", "password");
        Long johnId = saveUser(john);
        // Act
        underTest.deleteById(johnId);
        // Assert
        assertFalse(isUserExists(john));
    }

    @Test
    @DisplayName("It should add new user")
    void itShouldAddNewUser() {
        // Arrange
        AppUser kate = new AppUser(33L, "Kate", "secret");
        // Act
        underTest.save(kate);
        // Assert
        assertTrue(isUserExists(kate));
        // CleanUp
        removeUser(kate);
    }

    @Test
    @DisplayName("It should update user")
    void itShouldUpdateUser() {
        // Arrange
        AppUser bart = new AppUser(33L, "Bart", "secret");
        Long bartId = saveUser(bart);
        // Act
        bart.setPassword("password");
        underTest.save(bart);
        // Assert
        assertAll(
                () -> assertTrue(isUserExists(bart)),
                () -> assertEquals(bartId, bart.getId()),
                () -> assertEquals(bart.getPassword(), findUser(bart).getPassword())
        );
        // CleanUp
        removeUser(bart);
    }

    @Test
    @DisplayName("It should page and sort result of db scripts migration")
    void itShouldPageAndSortResult() {
        // Arrange - Act
        Page<AppUser> page = underTest.findAll(PageRequest.of(0, 5, Sort.by(DESC, "name")));
        // Assert
        assertAll(
                () -> assertEquals(2, page.getTotalElements()),
                () -> assertEquals(2, page.getNumberOfElements()),
                () -> assertEquals(1, page.getTotalPages()),
                () -> assertEquals("Jane White", page.getContent().get(0).getName()),
                () -> assertEquals("Harry Blue", page.getContent().get(1).getName())
        );
    }

}