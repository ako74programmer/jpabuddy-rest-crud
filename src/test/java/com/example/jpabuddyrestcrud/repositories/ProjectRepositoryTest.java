package com.example.jpabuddyrestcrud.repositories;

import com.example.jpabuddyrestcrud.entities.Project;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;

import java.util.List;

import static org.assertj.core.api.Assertions.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestPropertySource(properties = "classpath:application-test.properties")
@DisplayName("AppUserRepositoryTest: tests with H2 database in file")
class ProjectRepositoryTest {

    @Autowired
    private ProjectRepository underTest;

    @Autowired
    private TestEntityManager entityManager;

    private Project findProject(Project project) {
        return entityManager.find(Project.class, project.getId());
    }

    @Test
    @Sql(scripts = "insert-projects.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(scripts = "delete-projects.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    @DisplayName("It should find all projects")
    void itShouldFindAllProjects() {
        // Arrange-Act
        List<Project> projects = underTest.findAll();
        // Assert
        assertThat(projects).hasSize(1);
        assertThat(projects.get(0).getName()).isEqualTo("Project2");
        assertThat(projects.get(0).getManager().getId()).isEqualTo(2);

    }

}